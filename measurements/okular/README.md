This directory contains a measurement done at the [Umwelt-Campus Birkenfeld](https://www.umwelt-campus.de/en/institutes/iss).

There is the automation for the usage scenario as an Actiona configuration and the report about the measured data.
